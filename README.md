# Readme for pam-mkhomedir

## Upstream
None! This is an original package to the Devuan GNU+Linux community.

## Reason for being
To substitute for oddjob-mkhomedir which is required for freeipa-client. This package in no way uses oddjob or systemd or dbus. It exists to "provide" package name `oddjob-mkhomedir` to satisfy requirements for freeipa-client.

## Alternatives
On Devuan, manually run `pam-update-auth --package --enable mkhomedir`.
Elsewhere (which probably has systemd): install package oddjob-mkhomedir.

## Dependencies
Distro       | Packages
------------ | ----------------
Devuan Ceres | libpam-modules

## References

## Differences from upsteam
N/A
